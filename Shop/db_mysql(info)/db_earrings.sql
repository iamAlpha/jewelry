
USE `db`;

DROP TABLE IF EXISTS `earrings`;
CREATE TABLE `earrings` (
  `earrings_id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(45) NOT NULL,
  `store_name` varchar(45) NOT NULL,
  `brand_description` varchar(445) NOT NULL,
  `brand_price` varchar(45) NOT NULL,
  PRIMARY KEY (`earrings_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `earrings` VALUES (1,'EarringsAmethist','OdesaGold','Beutiful earrings with Amethist','1500'),
(2,'EarringsSamphire','LvivGold','Beutiful earrings with saphires','1800'),
(3,'EarringsRuby','HarkivGold','Beutiful earrings with rubys','2100'),
(4,'EarringsBriliant','KyivGold','Beutiful earrings with briliants ','2800');

