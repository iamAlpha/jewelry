
USE `db`;
DROP TABLE IF EXISTS `rings`; 
CREATE TABLE `rings` (
  `rings_id` int(11) NOT NULL AUTO_INCREMENT,
  `ring_name` varchar(45) NOT NULL,
  `author_name` varchar(45) NOT NULL,
  `ring_description` varchar(445) NOT NULL,
  `ring_price` varchar(25) NOT NULL,
  PRIMARY KEY (`rings_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

INSERT INTO `rings` VALUES (1,'RingAmethist','OdesaGold','Beutiful ring with Amethist','2000'),
(2,'RingSamphire','LvivGold','Beutiful ring with saphires','2300'),
(3,'RingRuby','HarkivGold','Beutiful ring with rubys','2700'),
(4,'RingBriliant','KyivGold','Beutiful ring with briliants ','3000');
