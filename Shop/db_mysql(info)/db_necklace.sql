
USE `db`;
DROP TABLE IF EXISTS `necklace`;
CREATE TABLE `necklace` (
  `necklace_id` int(11) NOT NULL AUTO_INCREMENT,
  `necklace_name` varchar(45) NOT NULL,
  `store_name` varchar(45) NOT NULL,
  `necklace_description` varchar(445) NOT NULL,
  `necklace_price` varchar(45) NOT NULL,
  PRIMARY KEY (`necklace_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `necklace` VALUES (1,'NecklaceAmethist','OdesaGold','Beutiful necklace with Amethist','2500'),
(2,'NecklaceSamphire','LvivGold','Beutiful necklace with saphires','2800'),
(3,'NecklaceRuby','HarkivGold','Beutiful necklace with rubys','3100'),
(4,'NecklaceBriliant','KyivGold','Beutiful necklace with briliants ','3800');
