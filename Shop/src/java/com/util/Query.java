package com.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.HashMap;
import java.util.logging.Logger;

public class Query {

    static Connection con;
    static Statement stmt;
    static PreparedStatement ps;
    static String query;
    static ResultSet rs;
    static ArrayList list = new ArrayList();
    static ProductDetails productdetails = new ProductDetails();  //class name instantiation. 
    static HashMap map = new HashMap();

    public static HashMap get_product_info(String classifier_name) {
        try {
            con = DAOConnection.sqlconnection();
            if (classifier_name.equalsIgnoreCase("rings")) {
                query = "select ring_name,author_name,ring_description,ring_price,rings_id from rings";
            }
            if (classifier_name.equalsIgnoreCase("earrings")) {
                query = "select brand_name,store_name,brand_description,brand_price,earrings_id from earrings";
            }
            if (classifier_name.equalsIgnoreCase("necklace")) {
                query = "select necklace_name,store_name,necklace_description,necklace_price,necklace_id from necklace";
            }
            ps = con.prepareStatement(query);
            rs = ps.executeQuery(query);
            ArrayList productname = new ArrayList();
            ArrayList brandname = new ArrayList();
            ArrayList productdescription = new ArrayList();
            ArrayList productprice = new ArrayList();
            ArrayList productid = new ArrayList();

            while (rs != null && rs.next()) {
                productname.add(rs.getString(1));       // using 4 objects to get 4 different values from a db         
                brandname.add(rs.getString(2));         // and storing it in a list
                productdescription.add(rs.getString(3));
                productprice.add(rs.getString(4));
                productid.add(rs.getString(5));
            }
            productdetails.setProduct_name(productname);
            System.out.println("Getting product name from get_product_info = " + productdetails.getProduct_name());
            productdetails.setProduct_brand(brandname);
            System.out.println("Getting product brand from get_product_info = " + productdetails.getProduct_brand());
            productdetails.setProduct_description(productdescription);
            System.out.println("Getting product description from get_product_info = " + productdetails.getProduct_description());
            productdetails.setProduct_price(productprice);
            System.out.println("Getting product price from get_product_info = " + productdetails.getProduct_price());


            map.put("productname", productname);      //list values are stored in a map so, that, we could  
            map.put("brandname", brandname);          //return the map object there by all the 4 list objects are passed. 
            map.put("productdescription", productdescription);
            map.put("productprice", productprice);
            map.put("productid",productid);
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                    System.out.println("Conntection Terminated after retrieving the product information");
                } catch (SQLException ex) {
                    Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return map;
    }

    public void insert_data(String classifier_name) {
        try {
            con = DAOConnection.sqlconnection();
            if (classifier_name.equals("rings")) {
                query = "select ring_name,store_name,ring_description,ring_price from rings";
            }
            if (classifier_name.equals("earrings")) {
                query = "select brand_name,store_name,brand_description,brand_price from earrings";
            }
            if (classifier_name.equals("necklace")) {
                query = "select necklace_name,store_name,necklace_description,necklace_price from necklace";
            }
            ps = con.prepareStatement(query);
            rs = ps.executeQuery(query);
            ArrayList productname = new ArrayList();
            ArrayList brandname = new ArrayList();
            ArrayList productdescription = new ArrayList();
            ArrayList productprice = new ArrayList();

            while (rs != null && rs.next()) {
                productname.add(rs.getString(1));       // using 4 objects to get 4 different values from a db         
                brandname.add(rs.getString(2));         // and storing it in a list
                productdescription.add(rs.getString(3));
                productprice.add(rs.getString(4));
            }
            productdetails.setProduct_name(productname);
            System.out.println("Getting product name from get_product_info = " + productdetails.getProduct_name());
            productdetails.setProduct_brand(brandname);
            System.out.println("Getting product brand from get_product_info = " + productdetails.getProduct_brand());
            productdetails.setProduct_description(productdescription);
            System.out.println("Getting product description from get_product_info = " + productdetails.getProduct_description());
            productdetails.setProduct_price(productprice);
            System.out.println("Getting product price from get_product_info = " + productdetails.getProduct_price());


            map.put("productname", productname);      //list values are stored in a map so, that, we could  
            map.put("brandname", brandname);          //return the map object there by all the 4 list objects are passed. 
            map.put("productdescription", productdescription);
            map.put("productprice", productprice);
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                    System.out.println("Conntection Terminated after retrieving the product information");
                } catch (SQLException ex) {
                    Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public String insert_data_into_db(String classifier_name, String admin_product_name, String admin_product_author_store_name, String admin_product_description, String admin_product_price) {
        String insertion_result = "";
        System.out.println("admin_product_price = " + admin_product_price);
        System.out.println("admin_product_author_store_name = " + admin_product_author_store_name);
        System.out.println("admin_product_description = " + admin_product_description);
        System.out.println("admin_product_name = " + admin_product_name);
        try {
            con = DAOConnection.sqlconnection();

            if (classifier_name.equals("Rings")) {
                query = " insert into rings values (?,?,?,?,?)";
            }
            if (classifier_name.equals("Earrings")) {
                query = " insert into earrings values (?,?,?,?,?)";
            }
            if (classifier_name.equals("Necklace")) {
                query = " insert into necklace values (?,?,?,?,?)";
            }

            ps = con.prepareStatement(query);
            ps.setString(1, null);
            ps.setString(2, admin_product_name);
            ps.setString(3, admin_product_author_store_name);
            ps.setString(4, admin_product_description);
            ps.setString(5, admin_product_price);
            int i = ps.executeUpdate();

            if (i > 0) {
                System.out.println("Insertion into database done successfully!");
                insertion_result = "success";

            } else {
                System.out.println("Failed to insert into database");
                insertion_result = "failure";
            }
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                    System.out.println("Conntection Terminated after sucessful/failure insertion into db");
                } catch (SQLException ex) {
                    Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return insertion_result;
    }

    
    String remove_data(String classifier_name, String admin_radio_selection, String admin_id_or_name) {
        String db_delete_result = "";
        System.out.println("admin_radio_selection = " + admin_radio_selection);
        System.out.println("admin_id_or_name = " + admin_id_or_name);
        System.out.println("classifier_name = " + classifier_name);
        try {
            con = DAOConnection.sqlconnection();
            stmt = con.createStatement();
            if (classifier_name.equalsIgnoreCase("rings")) {
                if (admin_radio_selection.equals("id")) {
                    query = " DELETE FROM rings WHERE rings_id = '" + admin_id_or_name + "'";
                } else {
                    query = " DELETE FROM rings WHERE ring_name = '" + admin_id_or_name + "'";
                }
            }
            if (classifier_name.equalsIgnoreCase("earrings")) {
                if (admin_radio_selection.equals("id")) {
                    query = " DELETE FROM earrings WHERE earrings_id = '" + admin_id_or_name + "'";
                } else {
                    query = " DELETE FROM earrings WHERE brand_name = '" + admin_id_or_name + "'";
                }
            }
            if (classifier_name.equalsIgnoreCase("necklace")) {
                if (admin_radio_selection.equals("id")) {
                    query = " DELETE FROM necklace WHERE necklace_id = '" + admin_id_or_name + "'";
                } else {
                    query = " DELETE FROM necklace WHERE necklace_name = '" + admin_id_or_name + "'";
                }
            }
            
            int i = stmt.executeUpdate(query);
            

            if (i > 0) {
                System.out.println("Deleted from database successfully!");
                db_delete_result = "success";

            } else {
                System.out.println("Failed to delete from database");
                db_delete_result = "failure";
            }
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                    System.out.println("Conntection Terminated after sucessful/failure deletion from db");
                } catch (SQLException ex) {
                    Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return db_delete_result;
    }
}// class query ends here..
